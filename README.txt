This module provides a way to store and display community group data from The 
City. Churches can use this module to display information about their community 
groups on their site.
