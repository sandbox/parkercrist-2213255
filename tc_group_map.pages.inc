<?php

/**
 * @file
 * Page callbacks for the The City Group Map.
 */

/**
 * Sample group request form.
 */
function tc_group_map_group_request_form($form, &$form_state, $group, $request_type) {
  $form_state['#group'] = $group;
  $form['request-type'] = array(
    '#type' => 'value',
    '#value' => $request_type,
  );

  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Your Email:'),
    '#required' => TRUE,
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Your Name:'),
  );

  $form['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Your Message:'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Send Message',
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 *
 * Form validation callback.
 */
function tc_group_map_group_request_form_validate($form, &$form_state) {
  if (!valid_email_address($form_state['values']['email'])) {
    form_set_error('email', t('This e-mail address is invalid.'));
  }
}

/**
 * Implements hook_form_submit()
 *
 * Form submission callback.
 */
function tc_group_map_group_request_form_submit($form, &$form_state) {
  $submission_info = $form_state['values'];
  $group = $form_state['#group'];

  $submission_info['group'] = $group;
  $submission_info['group_emails'] = $group->props->leaders;
  $submission_info['group_tags'] = $group->props->tags;

  module_invoke_all('tc_group_map_group_request', $submission_info);
  drupal_set_message('sent');
}

/**
 * Load sample data form.
 */
function tc_group_map_load_sample_data_form($form, &$form_state) {
  return confirm_form(
    $form,
    t('Are you sure you want to load sample groups?'),
    'admin/content/community-groups',
    t('No kittens will be hurt, but we just wanted to make sure.')
  );
}

/**
 * Implements hook_form_submit().
 *
 * Form submission callback.
 */
function tc_group_map_load_sample_data_form_submit($form, &$form_state) {
  $file = drupal_get_path('module', 'tc_group_map') . '/sample_data.json';
  if (!is_file($file)) {
    drupal_set_message(t('Sample file was not found at %path', array('%path' => $file)), 'error');
    return;
  }

  $data = json_decode(file_get_contents($file));
  foreach ($data->groups as $group) {
    $cnt = db_query('SELECT count(group_id) FROM {tc_group_map_groups} WHERE group_id = :group_id', array(':group_id' => $group->group_id))->fetchField();
    if ($cnt > 0) {
      continue;
    }
    $group->props = serialize($group->props);
    drupal_write_record('tc_group_map_groups', $group);
  }

  $form_state['redirect'] = 'admin/content/community-groups';
}
