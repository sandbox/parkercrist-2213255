<?php

/**
 * @file
 * Admin settings callbacks for the The City Group Map.
 */

/**
 * Admin settings form callback.
 */
function tc_group_map_settings_form($form, &$form_state) {
  $form['tc_group_map_google_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Map API Key'),
    '#default_value' => variable_get('tc_group_map_google_api_key', ''),
  );

  $form['tc_group_map_webhook_sig'] = array(
    '#type' => 'textfield',
    '#title' => t('The City Webhook Sig'),
    '#default_value' => variable_get('tc_group_map_webhook_sig', ''),
  );

  $form['tc_group_map_coordinates'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default Map Coordinates'),
    '#description' => t('The default coordinates used if the map is unable to center on the location of the user'),
    'tc_group_map_latitude' => array(
      '#type' => 'textfield',
      '#title' => t('Latitude'),
      '#default_value' => variable_get('tc_group_map_latitude', ''),
    ),
    'tc_group_map_longitude' => array(
      '#type' => 'textfield',
      '#title' => t('Longitude'),
      '#default_value' => variable_get('tc_group_map_longitude', ''),
    ),
  );

  $form['tc_group_map_marker_icon'] = array(
    '#type' => 'textfield',
    '#title' => t('Marker Icon URL'),
    '#description' => t('Link to image file to be used for map markers'),
    '#default_value' => variable_get('tc_group_map_marker_icon', ''),
  );

  return system_settings_form($form);
}

/**
 * CG Group admin form.
 */
function tc_group_map_groups_admin_form($form, &$form_state) {
  $form['add_link'] = array(
    '#markup' => '<ul class="action-links"><li>' . l(t('Load Sample Groups'), request_path() . '/load-sample-data') . '</li></ul>',
  );

  $form['groups'] = array(
    '#type' => 'tableselect',
    '#header' => array(
      'group_id',
      'title',
      'created',
      'modified',
      'deletion_scheduled',
      'unlisted',
      'Addresses',
    ),
    '#options' => array(),
    '#empty' => t('No groups found'),
  );

  $query = db_select('tc_group_map_groups', 'g')
    ->fields('g', array('group_id'));
  $result = $query->execute();
  foreach ($result as $row) {
    $group = tc_group_map_group_load($row->group_id);
    $addresses = '';
    foreach ($group->props->addresses as $address) {
      $address_txt = $address->street . ' ' . $address->city . ', ' . $address->state;
      $addresses .= l(
        $address_txt,
        '//maps.google.com',
        array(
          'external' => TRUE,
          'query' => array('q' => $address_txt),
          'attributes' => array('target' => '_blank', 'title' => check_plain($address_txt)),
        )
      ) . "<br />\n";
    }
    $form['groups']['#options'][$group->group_id] = array(
      l($group->group_id, request_path() . '/' . $group->group_id),
      l($group->props->name, request_path() . '/' . $group->group_id),
      format_date($group->created),
      format_date($group->modified),
      $group->props->deletion_scheduled || '',
      $group->props->unlisted || '',
      $addresses,
    );
  }

  $form['actions'] = array(
    '#type' => 'actions',
  );

  $form['actions']['operation'] = array(
    '#type' => 'select',
    '#title' => t('With Selected'),
    '#options' => array(
      'reload' => t('Reload from The City'),
      'unlist' => t('Unlist Group(s)'),
      'list' => t('List Group(s)'),
    ),
    '#required' => TRUE,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  $form['actions']['refresh_all'] = array(
    '#type' => 'submit',
    '#value' => t('Refresh All'),
    '#limit_validation_errors' => array(),
    '#submit' => array('tc_group_map_refresh_all_submission_callback'),
  );

  return $form;
}

/**
 * CG Group Admin form validation callback.
 */
function tc_group_map_admin_form_validate($form, &$form_state) {
  $groups = array_filter($form_state['values']['groups']);
  if (empty($groups)) {
    form_set_error('groups', t('You must select at least one group.'));
  }
  else {
    $form_state['values']['groups'] = $groups;
  }
}

/**
 * CG Group Admin form submission callback.
 */
function tc_group_map_admin_form_submit($form, &$form_state) {
  $groups = $form_state['values']['groups'];
  switch ($form_state['values']['operation']) {
    case 'reload':
      $count = 0;
      foreach ($groups as $group_id) {
        $group = tc_group_map_group_load($group_id);
        if ($props = tc_group_map_api_group_show($group->group_id)) {
          $errors = tc_group_map_validate_group($props);
          if (is_array($errors)) {
            drupal_set_message(t(
              'Encountered the following errors while updating %group: @errors',
              array(
                '%group' => $group_id,
                '@errors' => implode("\n", $errors)),
              ),
              'error'
            );
            continue;
          }
          $group->props = $props;
          tc_group_map_group_update($group);
        }
        else {
          drupal_set_message(t('Could not load group %group_id. Check watchdog for more details', array('%group_id' => $group->group_id)));
        }
        $count++;
      }
      drupal_set_message(format_plural($count, 'Updated 1 group', 'Updated @count groups', array('@count' => $count)));
      break;

    case 'unlist':
      foreach ($groups as $group_id) {
        $group = tc_group_map_group_load($group_id);
        $group->props->unlisted = TRUE;
        tc_group_map_group_update($group);
      }
      $count = count($groups);
      drupal_set_message(format_plural(
        $count,
        'Unlisted 1 group',
        'Unlisted @count groups',
        array('@count' => $count)
      ));
      break;

    case 'list':
      foreach ($groups as $group_id) {
        $group = tc_group_map_group_load($group_id);
        $group->props->unlisted = FALSE;
        tc_group_map_group_update($group);
      }
      $count = count($groups);
      drupal_set_message(format_plural(
        $count,
        'Listed 1 group',
        'Listed @count groups',
        array('@count' => $count)
      ));
      break;

  }
}

/**
 * Retrieves a cg's addresses from The City.
 */
function tc_group_map_api_addresses_index($group_id) {
  if ($path = libraries_get_path('city-api')) {
    require_once $path . '/sdk.php';
  }
  else {
    watchdog('tc_group_map', 'The City SDK path not found');
    return;
  }

  try {
    $ca = new JnetCityApi();
    $ca->json = TRUE;
    $result = $ca->groups_show($group_id);
    $group = json_decode($result);
    if (isset($group->error_message)) {
      throw new Exception($group->error_message);
    }
    return $group;
  }
  catch (Exception $e) {
    watchdog('tc_group_map', 'Encountered the following error while trying to load group %group_id: @error', array('%group_id' => $group_id, '@error' => $e->getMessage()));
    return FALSE;
  }
}

/**
 * Redirects to the refresh all form.
 */
function tc_group_map_refresh_all_submission_callback($form, &$form_state) {
  $form_state['redirect'] = array(
    request_path() . '/refresh-all',
    array(
      'query' => array('destination' => request_path()),
    ),
  );
}

/**
 * Refreshes CG's from The City (pulls all group info).
 */
function tc_group_map_refresh_all_form($form, &$form_state) {
  $form = confirm_form(
    $form,
    t('Refresh all group info?'),
    request_path(),
    t('Warning, this will override any current group info with group info from The City.')
  );

  return $form;
}

/**
 * Submission callback for refresh all form.
 */
function tc_group_map_refresh_all_form_submit($form, &$form_state) {
  if ($path = libraries_get_path('city-api')) {
    require_once $path . '/sdk.php';
  }
  else {
    watchdog('tc_group_map', 'The City SDK path not found');
    return;
  }

  $ca = new JnetCityApi();
  $ca->json = TRUE;
  $result = $ca->groups_count(array('group_types' => 'CG'));
  $result = json_decode($result);
  $per_page = 20;
  $operations = array();
  for ($i = 1; $i <= ceil($result->count / $per_page); $i++) {
    $operations[] = array(
      'tc_group_map_refresh_groups', array('page' => $i),
    );
  }
  $batch = array(
    'title' => t('Refreshing groups'),
    'progress_message' => 'Refreshing page @current of @total.',
    'operations' => $operations,
    'file' => drupal_get_path('module', 'tc_group_map') . '/includes/admin.inc',
  );
  batch_set($batch);
}

/**
 * Batch callback: Refreshes CG data from The City.
 */
function tc_group_map_refresh_groups($page, &$context) {
  if ($path = libraries_get_path('city-api')) {
    require_once $path . '/sdk.php';
  }
  else {
    watchdog('tc_group_map', 'The City SDK path not found');
    return;
  }

  module_load_include('inc', 'tc_group_map', 'tc_group_map.city_webhook');
  $ca = new JnetCityApi();
  $ca->json = TRUE;
  $result = $ca->groups_index(array(
    'group_types' => 'CG',
    'include_addresses' => 'true',
    'page' => $page,
  ));
  $result = json_decode($result);
  foreach ($result->groups as $group) {
    if (!empty($group->addresses)) {
      foreach ($group->addresses as &$address) {
        if (empty($address->latitude)) {
          tc_group_map_group_address_geocode($address);
        }
      }
    }
    $group->webhook_event = 'update';
    $group->webhook_object = 'Group';
    tc_group_map_city_webhook($group);
  }
}

/**
 * Single group page.
 */
function tc_group_map_group_page($group) {
  require_once DRUPAL_ROOT . '/includes/utility.inc';
  $output = array();
  $output['group_details'] = array(
    '#prefix' => '<pre>',
    '#suffix' => '</pre>',
    '#markup' => drupal_var_export($group),
  );

  return $output;
}
