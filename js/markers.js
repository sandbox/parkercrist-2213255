(function($) {
  Drupal.behaviors.tc_group_map = {
    // Store days in an object for easy comparison later.
    _days: {
      0: "Monday",
      1: "Tuesay",
      2: "Wednesday",
      3: "Thursday",
      4: "Friday",
      5: "Saturday",
      6: "Sunday",
      "Monday": "M",
      "Tuesday": "T",
      "Wednesday": "W",
      "Thursday": "R",
      "Friday": "F",
      "Saturday": "S",
      "Sunday": "S",
    },

    attach: function(context, settings) {
      // Initialize the map.
      var userLoc = new google.maps.LatLng(settings.tc_group_map.default_latitude, settings.tc_group_map.default_longitude);

      // If the user location is available, grab the latitude and longitude
      if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          userLoc = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
          if (map) {
            map.panTo(userLoc);
          }
        });
      }

      Drupal.behaviors.tc_group_map._markers = [];
      var infowindows = [],
      active_info = null,
      options = {
        'zoom': 14,
        'disableDefaultUI': true,
        'center': userLoc,
        'mapTypeId': google.maps.MapTypeId.ROADMAP,
        'maxZoom' : 16,
        'minZoom' : 9,
        'zoomControl' : true
      },
      map = new google.maps.Map(document.getElementById("map_canvas"), options);
      map.setOptions({
        styles: [{
          featureType: "all",
          stylers: "default"
        }]
      });

      // Create map markers.
      $.each(settings.tc_group_map.data_map_json, function(id, marker_data) {
        var plusOrMinus = Math.random() < 0.5 ? -1 : 1,
        randomLat = parseFloat(marker_data.latitude) + (plusOrMinus * (Math.random() * .002) + .001),
        randomLng = parseFloat(marker_data.longitude) + (plusOrMinus * (Math.random() * .002) + .001),
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(randomLat, randomLng),
          id: marker_data.marker_id,
          map: map,
          title: marker_data.title,
          icon: settings.tc_group_map.marker_icon,
          groupId: id,
          display: true,
          data: marker_data,
        });

        google.maps.event.addListener(marker, 'click', function(){
          if (active_info) {
            infowindows[active_info].close();
          }
          active_info = this.id;
          infowindows[this.id].open(this.map);

          // Uncomment to center on marker click.
          map.setCenter(marker.position);
          map.panTo(marker.position);
          return false;
        });

        Drupal.behaviors.tc_group_map._markers.push(marker);

        var myOptions = {
          content: Drupal.theme("markerWindow", marker_data),
          disableAutoPan: false,
          maxWidth: 0,
          alignBottom: true,
          pixelOffset: new google.maps.Size(0, -40),
          zIndex: null,
          boxClass: "info-windows",
          closeBoxURL: "",
          pane: "floatPane",
          enableEventPropagation: false,
          infoBoxClearance: "10px",
          position: marker.position
        };

        var infowindow = new InfoBox(myOptions);
        infowindows[marker.id] = infowindow;

      // end Create map markers.
      });

      // Onchange handlers for filters
      $("#select-church, #select-day").change(function(e) {
        if (active_info) {
          infowindows[active_info].close();
        }
        var church = $("#select-church").val(),
        day = $("#select-day").val();
        $.each(Drupal.behaviors.tc_group_map._markers, function(id, marker) {
          marker.setVisible(true);
          if(church != '_any' && marker.data.campus != church) {
            marker.setVisible(false);
            return;
          }

          if(day != '_any' && Drupal.behaviors.tc_group_map._days[marker.data.tags.Day] != day) {
            marker.setVisible(false);
            return;
          }
        });

      // end Onchange handlers for filters.
      });

      $("#tc-group-map-center").click(function(e) {
        e.preventDefault();

      })

    }, // end attach
  }

  /**
   * Themes a marker popup window.
   */
  Drupal.theme.prototype.markerWindow = function(marker) {
    var days = {
      "M": "Monday",
      "T": "Tuesday",
      "W": "Wednesday",
      "R": "Thursday",
      "F": "Friday",
      "S": "Saturday",
      "S": "Sunday"
    };
    var output = '';
    output += "<div style='width:308px' class='marker-window'>";
    output += "<h1>" + marker.title + "</h1>";
    output += "<h2 class='church-label'>" + marker.campus.replace(/^\s+|\s+$/g,'') + "</h2>";
    output += "<div class='dayBox label round' style='background: none !important; margin: 0px !important;'>";
    for (x in days) {
      var active = marker.tags.Day == days[x] ? "font-weight:bold;" : "";
      output += "<span style='" + active + "'>" + x + "</span>";
    }
    output += "</div>";
    output += "<br><br><br><a href='" + Drupal.settings.basePath + "tc-group-map/group/" + marker.id + "/join' class='joinGroup button small'>Request to Join</a>";
    output += "<a href='" + Drupal.settings.basePath + "tc-group-map/group/" + marker.id + "/report' class='reportError button small'>Report Incorrect Info</a>";
    output += "</div>";
    return output;
  }

})(jQuery);
