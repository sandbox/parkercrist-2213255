<?php

/**
 * @file
 * Webhook callback for the The City Group Map.
 */

/**
 * Receives webhook call from The City.
 *
 * The City POST's events directly to this callback as
 * json data.  We're only concerned about groups and
 * addresses at this point.
 *
 * @see https://api.onthecity.org/docs/webhooks#api_endpoints
 * @see https://journeyon.onthecity.org/admin/api/city_api_keys?subnav_state=settings_api_keys
 */
function tc_group_map_city_webhook($event = NULL) {
  // Retrieve the request's body and parse it as JSON.
  if (empty($event)) {
    $body = @file_get_contents('php://input');
    $event = json_decode($body);
  }
  if (!is_object($event) || !isset($event->webhook_object) || !isset($event->webhook_event)) {
    return;
  }

  $record = (object) array(
    'webhook_object_id' => $event->id,
    'webhook_object_type' => $event->webhook_object,
    'webhook_object_event' => $event->webhook_event,
    'timestamp' => time(),
    'webhook_object_data' => serialize($event),
  );
  drupal_write_record('tc_group_map_webhook_log', $record);

  switch ($event->webhook_object) {
    case 'Group':
      switch ($event->webhook_event) {
        case 'create':
          // The only reason I'm not doing anything with create right now
          // is because new groups don't have any addresses and we only really
          // care about groups that have addresses at this point.
          break;

        case 'update':
          $errors = tc_group_map_validate_group($event);
          if (is_array($errors)) {
            tc_group_map_group_delete($event->id);
            watchdog(
              'tc_group_map',
              'Encountered the following errors while trying to update %group: @errors',
              array('%group' => $event->id, '@errors' => implode("\n", $errors))
            );
          }
          else {
            // If group exists, update. if not, create.
            $cnt = db_query('SELECT count(group_id) from {tc_group_map_groups} WHERE group_id = :gid', array(':gid' => $event->id))->fetchField();
            tc_group_map_group_update($event, $cnt == 0);
          }
          break;

      }
      break;

    case 'Address':
      switch ($event->webhook_event) {
        case 'create':
        case 'update':
          $group = tc_group_map_group_load($event->group_id);
          if (!$group) {
            $group = tc_group_map_api_group_show($event->group_id);
            $errors = tc_group_map_validate_group($group);
            if (!empty($errors)) {
              $group = FALSE;
            }
            else {
              tc_group_map_group_update($group, TRUE);
            }
          }
          if ($group) {
            // If this is not a public address, don't show it.
            if ($event->privacy != 'Public') {
              unset($group->props->addresses[$event->id]);
              tc_group_map_group_update($group);
              break;
            }
            unset($event->changed);

            // If no lat/lng was set, try to geocode from Google.
            if (empty($event->latitude)) {
              tc_group_map_group_address_geocode($event);
            }
            $group->props->addresses[$event->id] = $event;
            tc_group_map_group_update($group);
          }
          else {
            watchdog('tc_group_map', 'Received webhook to @event an address but the group: %group does not exist, we could not load it from The City, or it did not validate.', array('@event' => $event->webhook_event, '%group' => $event->group_id));
          }
          break;

        case 'destroy':
          if ($group = tc_group_map_group_load($event->group_id)) {
            unset($group->props->addresses[$event->id]);
            tc_group_map_group_update($group);
          }
          break;

      }
      break;
  }
}

/**
 * Strips unsafe characters from group name.
 */
function tc_group_map_clean_group_name($group_name) {
  // First, replace UTF-8 characters.
  $group_name = str_replace(
    array(
      "\xe2\x80\x98",
      "\xe2\x80\x99",
      "\xe2\x80\x9c",
      "\xe2\x80\x9d",
      "\xe2\x80\x93",
      "\xe2\x80\x94",
      "\xe2\x80\xa6",
    ),
    array("'", "'", '"', '"', '-', '--', '...'),
    $group_name
    );
  // Next, replace their Windows-1252 equivalents.
  $group_name = str_replace(
    array(chr(145), chr(146), chr(147), chr(148), chr(150), chr(151), chr(133)),
    array("'", "'", '"', '"', '-', '--', '...'),
    $group_name
    );
  $group_name = htmlspecialchars($group_name, ENT_QUOTES);

  return $group_name;
}
